from project.settings import MODELTRANS_AVAILABLE_LANGUAGES as LANGUAGES


def translation_fields(model):
    fields = []
    for field in model.get_translation_fields():
        for lang in LANGUAGES:
            fields.append(str(field) + '_' + str(lang))

    return fields
