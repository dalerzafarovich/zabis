from datetime import datetime

from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Order, OrderItem
from .serializers import OrderSerializer
from product.models import ProductPrice


class OrderListView(APIView):
    def get(self, request):

        orders = Order.objects.all().select_related('shop', 'user').prefetch_related(
            'orderitem_set', 'orderitem_set__product_price')

        if 'status' in request.GET:
            orders = orders.filter(status=request.GET.get('status'))

        return Response(OrderSerializer(orders, context={'request': request}, many=True).data)

    def post(self, request):
        data = request.data
        orderitems = data.get('orderitem_set')
        status = data.get('status')

        if 'orderitem_set' in data:
            product_price_ids = [orderitem['product_price'] for orderitem in orderitems]
            product_price_list = dict({p_id: p_price
                                       for p_id, p_price in
                                       ProductPrice.objects.filter(id__in=product_price_ids).values_list('id',
                                                                                                         'real_price')})

        if order_id := data.get('order_id'):
            order = Order.objects.get(pk=order_id)

            if ('orderitem_set' not in data) and status:
                order.update_status(status, datetime.now())
                order.save()
                return Response({'id': order.id, 'status': status, status+'_at': order.__getattribute__(status+'_at')})

            return self.update_order(request, order, orderitems, product_price_ids, product_price_list, status)
        return self.create_order(request, data, orderitems, product_price_list)

    @staticmethod
    def create_order(request, data, orderitems, product_price_list):
        total_sum = sum(
            (_order_item['count'] * product_price_list[_order_item['product_price']] for _order_item in orderitems))
        order = Order(user=request.user, shop_id=data['shop'], status='created', total_sum=total_sum,
                      created_at=datetime.now())
        order.save()
        orderitems_list = []

        for orderitem in orderitems:
            count = orderitem['count']
            product_price = product_price_list.get(orderitem['product_price'])
            total_sum = count * product_price
            orderitems_list.append(
                OrderItem(order=order, product_price_id=orderitem['product_price'], count=count, total_sum=total_sum))

        OrderItem.objects.bulk_create(orderitems_list)

        return Response(OrderSerializer(order, context={'request': request}).data, status=201)

    def update_order(self, request, order, orderitems, product_price_ids, product_price_list, status):
        orderitems_old = order.orderitem_set.all().select_related('product_price')
        product_price_ids_old = [orderitem.product_price.id for orderitem in orderitems_old]
        items_to_create = list()
        items_to_update = set()
        items_to_delete = set()
        order_total_sum = 0

        for item in orderitems:
            if item['product_price'] not in product_price_ids_old:
                items_to_create.append(item)

        for item in orderitems_old:
            if (price_id := item.product_price.id) not in product_price_ids:
                items_to_delete.add(item)
            else:
                for value in orderitems:
                    if value['product_price'] == price_id:
                        item.count = value['count']
                        item.total_sum = value['count'] * product_price_list.get(value['product_price'])
                        order_total_sum += item.total_sum
                        items_to_update.add(item)

        lst = []
        for item in items_to_create:
            count = item['count']
            product_price = product_price_list.get(item['product_price'])
            total_sum = count * product_price
            order_total_sum += total_sum
            lst.append(
                OrderItem(order=order, product_price_id=item['product_price'], count=count,
                          total_sum=total_sum))

        OrderItem.objects.bulk_create(lst)
        OrderItem.objects.bulk_update(items_to_update, ['count', 'total_sum'])
        OrderItem.objects.filter(id__in=[item.id for item in items_to_delete]).delete()
        order.total_sum = order_total_sum
        if status:
            order.update_status(status, datetime.now())
        order.save()
        return Response(OrderSerializer(order, context={'request': request}).data)


class OrderDetailView(APIView):
    def get(self, request, pk):
        order = get_object_or_404(Order, pk=pk)
        return Response(OrderSerializer(order, context={'request': request}).data)

    def delete(self, request, pk):
        order = get_object_or_404(Order, pk=pk)
        order.status = 'rejected'
        order.rejected_comment = request.data.get('rejected_comment')
        order.rejected_user = request.user
        order.rejected_at = datetime.now()
        order.save()
        return Response(OrderSerializer(order, context={'request': request}).data)
