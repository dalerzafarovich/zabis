from rest_framework import serializers
from .models import Order, OrderItem


class OrderItemSerializer(serializers.ModelSerializer):
    total_sum = serializers.ReadOnlyField()

    class Meta:
        model = OrderItem
        fields = ('id', 'product_price', 'count', 'total_sum')


class OrderSerializer(serializers.ModelSerializer):
    orderitem_set = OrderItemSerializer(many=True)

    class Meta:
        model = Order
        fields = '__all__'
