from django.db import models
from user.models import User
from shop.models import Shop
from .choicess import ORDER_STATUS_CHOICES as STATUS_CHOICES
from product.models import ProductPrice


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Пользователь')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    shop = models.ForeignKey(Shop, on_delete=models.PROTECT, verbose_name='Магазин')
    status = models.CharField(max_length=255, choices=STATUS_CHOICES, verbose_name='Статус')
    total_sum = models.BigIntegerField()

    confirmed_at = models.DateTimeField(null=True, blank=True)
    accepted_at = models.DateTimeField(null=True, blank=True)
    shipped_at = models.DateTimeField(null=True, blank=True)
    delivered_at = models.DateTimeField(null=True, blank=True)
    closed_at = models.DateTimeField(null=True, blank=True)
    rejected_at = models.DateTimeField(null=True, blank=True)

    rejected_comment = models.CharField(max_length=255, null=True, blank=True)
    rejected_user = models.ForeignKey(User, related_name='rejected_orders', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def update_status(self, status, time):
        self.status = status
        self.__setattr__(status + '_at', time)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.PROTECT, verbose_name='Заказ')
    product_price = models.ForeignKey(ProductPrice, on_delete=models.PROTECT, verbose_name='Цена')
    count = models.FloatField(verbose_name='Количество')
    total_sum = models.BigIntegerField(verbose_name='Общая сумма')

    class Meta:
        verbose_name = 'Предмет заказа'
        verbose_name_plural = 'Предметы заказа'
