ORDER_STATUS_CHOICES = (
    ('created', 'создан'),
    ('confirmed', 'подтвержден'),
    ('accepted', 'принят'),
    ('shipped', 'загружен'),
    ('delivered', 'доставлен'),
    ('closed', 'завершен'),
    ('rejected', 'отменен')
)
