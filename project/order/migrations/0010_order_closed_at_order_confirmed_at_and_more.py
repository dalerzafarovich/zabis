# Generated by Django 4.0.2 on 2022-02-25 11:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0009_order_rejected_user_alter_order_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='closed_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='confirmed_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='delivered_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='shipped_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('created', 'создан'), ('confirmed', 'подтвержден'), ('accepted', 'принят'), ('shipped', 'загружен'), ('delivered', 'доставлен'), ('closed', 'завершен'), ('rejected', 'отменен')], max_length=255, verbose_name='Статус'),
        ),
    ]
