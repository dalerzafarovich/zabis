from rest_framework import viewsets
from rest_framework.response import Response

from .models import Shop
from .serializers import ShopSerializer


class ShopViewSet(viewsets.ModelViewSet):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer

    def create(self, request, *args, **kwargs):
        data = dict(request.data)
        model_id = data.get('shop_id')
        if model_id:
            return self.update_model(request)
        return super().create(request, *args, **kwargs)

    def update_model(self, request):
        data = request.data
        model_id = data.pop('shop_id')
        queryset = self.get_queryset()
        model = queryset.get(pk=model_id)
        serializer = self.get_serializer_class()()
        serializer.update(model, data)
        return Response({'id': model_id, **data})
