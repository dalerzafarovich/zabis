from django.db import models

from user.models import User

from choicess import STATUS_CHOICES


class Shop(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Пользователь')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    title = models.CharField(max_length=255, verbose_name='Описание')
    contact_phone = models.CharField(max_length=255, verbose_name='Контактный телефон')
    contact_address = models.CharField(max_length=255, verbose_name='Контактный адрес')

    long = models.CharField(max_length=255, verbose_name='Долгота')
    lat = models.CharField(max_length=255, verbose_name='Широта')

    start_time = models.TimeField(verbose_name='Открывается')
    end_time = models.TimeField(verbose_name='Закрывается')

    status = models.CharField(max_length=255, choices=STATUS_CHOICES)

    class Meta:
        verbose_name = 'Магазин'
        verbose_name_plural = 'Магазины'
