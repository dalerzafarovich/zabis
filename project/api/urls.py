from django.urls import path
from provider.views import ProviderViewSet, NewsViewSet, RegionViewSet, OrganizationViewSet
from product.views import CategoryViewSet, ProductViewSet, ProductPriceViewSet
from shop.views import ShopViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'providers', ProviderViewSet)
router.register(r'news', NewsViewSet)
router.register(r'regions', RegionViewSet)
router.register(r'organizations', OrganizationViewSet)

router.register(r'categories', CategoryViewSet)
router.register(r'products', ProductViewSet)
router.register(r'product-prices', ProductPriceViewSet)

router.register(r'shops', ShopViewSet)

urlpatterns = router.urls
