from django.contrib import admin
from .models import Product, Category, ProductPrice
from functions import translation_fields


@admin.register(Product)
class ProductPriceAdmin(admin.ModelAdmin):
    fields = translation_fields(Product)
    fields += ['provider', 'status', 'image', 'category', 'unit_type']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = translation_fields(Category)
    fields += ['prior', 'image']


@admin.register(ProductPrice)
class ProductPriceAdmin(admin.ModelAdmin):
    pass
