from rest_framework.response import Response
from .serializers import CategorySerializer, ProductSerializer, ProductPriceSerializer
from .models import Category, Product, ProductPrice
from rest_framework import viewsets


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def create(self, request, *args, **kwargs):
        data = dict(request.data)
        model_id = data.get('category_id')
        if model_id:
            return self.update_model(request)
        return super().create(request, *args, **kwargs)

    def update_model(self, request):
        data = request.data
        model_id = data.pop('category_id')
        queryset = self.get_queryset()
        model = queryset.get(pk=model_id)
        serializer = self.get_serializer_class()()
        serializer.update(model, data)
        return Response({'id': model_id, **data})


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().prefetch_related('productprice_set')
    serializer_class = ProductSerializer

    def create(self, request, *args, **kwargs):
        data = dict(request.data)
        model_id = data.get('product_id')
        if model_id:
            return self.update_model(request)
        return super().create(request, *args, **kwargs)

    def update_model(self, request):
        data = request.data
        model_id = data.pop('product_id')
        queryset = self.get_queryset()
        model = queryset.get(pk=model_id)
        serializer = self.get_serializer_class()()
        serializer.update(model, data)
        return Response({'id': model_id, **data})


class ProductPriceViewSet(viewsets.ModelViewSet):
    queryset = ProductPrice.objects.all().select_related('product')
    serializer_class = ProductPriceSerializer

    def create(self, request, *args, **kwargs):
        data = dict(request.data)
        model_id = data.get('product_price_id')
        if model_id:
            return self.update_model(request)
        return super().create(request, *args, **kwargs)

    def update_model(self, request):
        data = request.data
        model_id = data.pop('product_price_id')
        queryset = self.get_queryset()
        model = queryset.get(pk=model_id)
        serializer = self.get_serializer_class()()
        serializer.update(model, data)
        return Response({'id': model_id, **data})
