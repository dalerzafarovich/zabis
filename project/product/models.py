from django.db import models
from modeltrans.fields import TranslationField
from provider.models import Provider
from choicess import STATUS_CHOICES

from choicess import UNIT_TYPE_CHOICES


class Category(models.Model):
    prior = models.IntegerField()
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='categories_images/')

    translation_fields = ('name',)
    i18n = TranslationField(fields=translation_fields)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name_i18n

    @classmethod
    def get_translation_fields(cls):
        return cls.translation_fields


class Product(models.Model):
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    status = models.CharField(max_length=255, choices=STATUS_CHOICES)
    image = models.ImageField(upload_to='products_images/')
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    rating = models.FloatField(null=True, blank=True)
    unit_type = models.CharField(max_length=255, choices=UNIT_TYPE_CHOICES, verbose_name='Единица измерения')

    translation_fields = ('title', 'description')
    i18n = TranslationField(fields=translation_fields)

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return self.title_i18n

    @classmethod
    def get_translation_fields(cls):
        return cls.translation_fields


class ProductPrice(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT, verbose_name='Продукт')
    min_amount = models.IntegerField(verbose_name='Минимальное количество')
    is_sale = models.BooleanField(verbose_name='Скидка')
    old_price = models.IntegerField(verbose_name='Старая цена')
    real_price = models.IntegerField(verbose_name='Новая цена')
    start_date = models.DateField(verbose_name='Начало')
    end_date = models.DateField(verbose_name='Конец')

    class Meta:
        verbose_name = 'Цена на продукт'
        verbose_name_plural = 'Цены на продукты'
