from rest_framework import serializers
from .models import Category, Product, ProductPrice


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'prior', 'name_ru', 'name_uz', 'image']


class ProductPriceInfo(serializers.ModelSerializer):
    class Meta:
        model = ProductPrice
        exclude = ['product']


class ProductSerializer(serializers.ModelSerializer):
    created_at = serializers.ReadOnlyField()
    updated_at = serializers.ReadOnlyField()
    product_prices = ProductPriceInfo(source='productprice_set', many=True, read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'provider', 'created_at', 'updated_at', 'status', 'image', 'title_ru', 'title_uz',
                  'description_ru', 'description_uz', 'category', 'rating', 'unit_type', 'product_prices']


class ProductPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductPrice
        fields = '__all__'
