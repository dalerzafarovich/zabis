from rest_framework import routers
from .views import ProductViewSet, CategoryViewSet, ProductPriceViewSet

router = routers.DefaultRouter()
router.register(r'categories', CategoryViewSet)
router.register(r'products', ProductViewSet)
router.register(r'product_price', ProductPriceViewSet)

urlpatterns = router.urls