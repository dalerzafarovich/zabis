from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models

from choicess import USER_ROLE_CHOICES


class User(AbstractUser):
    role = models.CharField(max_length=255, choices=USER_ROLE_CHOICES)
    organization = models.ForeignKey(to='provider.Organization', on_delete=models.SET_NULL, null=True,
                                     related_name='stuff')

    objects = UserManager()
