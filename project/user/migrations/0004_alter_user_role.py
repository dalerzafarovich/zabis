# Generated by Django 4.0.2 on 2022-02-24 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_alter_user_managers_alter_user_role'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='role',
            field=models.CharField(choices=[('shop', 'магазин'), ('provider', 'провайдер'), ('admin', 'админ'), ('provider_owner', 'владелец провайдера')], max_length=255),
        ),
    ]
