# Generated by Django 4.0.2 on 2022-02-24 13:41

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('provider', '0006_provider_stuff_organization'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='provider',
            name='user',
        ),
        migrations.AddField(
            model_name='provider',
            name='organization',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='provider.organization'),
        ),
        migrations.AlterField(
            model_name='organization',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='organizations', to=settings.AUTH_USER_MODEL, verbose_name='Владелец'),
        ),
    ]
