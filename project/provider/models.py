from datetime import timedelta
from django.db import models
from modeltrans.fields import TranslationField
from user.models import User

from choicess import STATUS_CHOICES


class Region(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название региона')

    translation_fields = ('name',)
    i18n = TranslationField(fields=translation_fields)

    def __str__(self):
        return self.name_i18n

    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'

    @classmethod
    def get_translation_fields(cls):
        return cls.translation_fields


class Organization(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название')
    owner = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Владелец', related_name='organizations')
    phone_number = models.CharField(max_length=255, null=True, blank=True, verbose_name='Номер телефона')
    requisites = models.TextField(null=True, blank=True, verbose_name='Реквизиты')
    INN = models.CharField(max_length=255, null=True, blank=True, verbose_name='ИНН')

    class Meta:
        verbose_name = 'Организация'
        verbose_name_plural = 'Организации'

    def __str__(self):
        return self.name


class Provider(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT, null=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    title = models.CharField(max_length=255, verbose_name='Название')
    logo = models.ImageField(upload_to='provider_logos/', verbose_name='Логотип')
    phone_number = models.CharField(max_length=255, verbose_name='Номер телефона')
    description = models.CharField(max_length=500, verbose_name='Описание')
    region = models.ForeignKey(Region, on_delete=models.PROTECT, verbose_name='Регион')
    status = models.CharField(max_length=255, choices=STATUS_CHOICES, verbose_name='Статус')
    stuff = models.ManyToManyField(User, related_name='providers')

    translation_fields = ('description',)
    i18n = TranslationField(fields=translation_fields)

    class Meta:
        verbose_name = 'Провайдер'
        verbose_name_plural = 'Провайдеры'

    def __str__(self):
        return self.title

    @classmethod
    def get_translation_fields(cls):
        return cls.translation_fields


class News(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    image = models.ImageField(upload_to='news_images/', verbose_name='Картинка')
    title = models.CharField(max_length=255, verbose_name='Заголовок')
    content = models.TextField(verbose_name='Контент')
    provider = models.ForeignKey(Provider, on_delete=models.PROTECT, verbose_name='Провайдер')

    translation_fields = ('title', 'content')
    i18n = TranslationField(fields=translation_fields)

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    def __str__(self):
        return self.title_i18n

    @classmethod
    def get_translation_fields(cls):
        return cls.translation_fields

    @property
    def show_end_time(self):
        return self.created_at + timedelta(days=1)
