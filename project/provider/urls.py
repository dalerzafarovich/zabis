from django.urls import path
from rest_framework import routers
from .views import RegionViewSet, ProviderViewSet, NewsViewSet, OrganizationViewSet

router = routers.DefaultRouter()
router.register(r'regions', RegionViewSet)
router.register(r'providers', ProviderViewSet)
router.register(r'news', NewsViewSet)
router.register(r'organizations', OrganizationViewSet)

urlpatterns = router.urls
