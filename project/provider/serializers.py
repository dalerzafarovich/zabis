from rest_framework import serializers
from .models import Region, Provider, News, Organization
from user.models import User


class OrganizationOwnerSerializer(serializers.ModelSerializer):
    role = serializers.ReadOnlyField()
    is_active = serializers.ReadOnlyField()

    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email', 'role', 'is_active')

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data, role='provider_owner', is_active=False)
        return user


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ('id', 'name_ru', 'name_uz')


class OrganizationSerializer(serializers.ModelSerializer):
    owner = OrganizationOwnerSerializer()

    class Meta:
        model = Organization
        fields = '__all__'


class ProviderSerializer(serializers.ModelSerializer):
    organization = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Provider
        fields = (
            'id', 'organization', 'created_at', 'updated_at', 'title', 'logo', 'phone_number', 'description_ru',
            'description_uz',
            'region', 'status', 'stuff')


class NewsSerializer(serializers.ModelSerializer):
    created_at = serializers.ReadOnlyField()
    show_end_time = serializers.ReadOnlyField()

    class Meta:
        model = News
        fields = (
            'id', 'created_at', 'show_end_time', 'image', 'title_ru', 'title_uz', 'content_ru', 'content_uz',
            'provider')
