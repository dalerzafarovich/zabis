from django.contrib import admin
from .models import Region, Provider, News, Organization
from functions import translation_fields


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    fields = translation_fields(Region)


@admin.register(Provider)
class ProviderAdmin(admin.ModelAdmin):
    fields = ['title']
    fields += translation_fields(Provider)
    fields += ['organization', 'logo', 'phone_number', 'region', 'status', 'stuff']


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    pass


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    fields = translation_fields(News)
    fields += ['image', 'provider']
