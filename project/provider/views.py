from rest_framework import viewsets, permissions
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from .serializers import RegionSerializer, ProviderSerializer, NewsSerializer, OrganizationSerializer, \
    OrganizationOwnerSerializer
from .models import Region, Provider, News, Organization


class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer

    def create(self, request, *args, **kwargs):
        data = dict(request.data)
        model_id = data.get('region_id')
        if model_id:
            return self.update_model(request)
        return super().create(request, *args, **kwargs)

    def update_model(self, request):
        data = request.data
        model_id = data.pop('region_id')
        queryset = self.get_queryset()
        model = queryset.get(pk=model_id)
        serializer = self.get_serializer_class()()
        serializer.update(model, data)
        return Response({'id': model_id, **data})


class ProviderViewSet(viewsets.ModelViewSet):
    queryset = Provider.objects.all().prefetch_related('stuff')
    serializer_class = ProviderSerializer

    def create(self, request, *args, **kwargs):
        data = dict(request.data)

        try:
            provider_id = data.pop('provider_id')
        except KeyError:
            return super().create(request, *args, **kwargs)

        provider = Provider.objects.get(pk=provider_id)
        serializer = ProviderSerializer()
        serializer.update(provider, data)

        return Response({'id': provider_id, **data})

    def perform_create(self, serializer):
        serializer.save(organization=self.request.user.organization)


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().select_related('provider')
    serializer_class = NewsSerializer

    def create(self, request, *args, **kwargs):
        data = dict(request.data)
        model_id = data.get('news_id')
        if model_id:
            return self.update_model(request)
        return super().create(request, *args, **kwargs)

    def update_model(self, request):
        data = request.data
        model_id = data.pop('news_id')
        queryset = self.get_queryset()
        model = queryset.get(pk=model_id)
        serializer = self.get_serializer_class()()
        serializer.update(model, data)
        return Response({'id': model_id, **data})


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all().select_related('owner')
    serializer_class = OrganizationSerializer

    def create(self, request, *args, **kwargs):
        data = dict(request.data)
        organization_id = data.get('organization_id')

        if organization_id:
            return self.update_model(request)

        owner_data = data.pop('owner')
        owner_serializer = OrganizationOwnerSerializer(data=owner_data)
        owner_serializer.is_valid()
        owner = owner_serializer.create(owner_data)
        organization = Organization(**data, owner=owner)
        organization.save()
        return Response(OrganizationSerializer(organization).data)

    def update_model(self, request):
        data = request.data
        model_id = data.pop('organization_id')
        queryset = self.get_queryset()
        model = queryset.get(pk=model_id)
        serializer = self.get_serializer_class()()
        serializer.update(model, data)
        return Response({'id': model_id, **data})
