STATUS_CHOICES = (
    ('active', 'активен'),
    ('inactive', 'неактивен')
)

USER_ROLE_CHOICES = (
    ('shop', 'магазин'),
    ('distributor', 'дистрибьютор'),
    ('admin', 'админ'),
    ('provider_owner', 'владелец провайдера')
)

UNIT_TYPE_CHOICES = (
    ('kg', 'килограмы'),
    ('piece', 'штуки'),
)
